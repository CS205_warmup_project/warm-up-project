from os import error
from query import *
from interface import *
import csv
import re

# artist commands validation
def artist_validation(enter):
    
    # open csv file and read artist name column into a list
    with open("artists.csv", "r") as csv_file:
        lines = csv_file.readlines()
    artist_name = []
    for line in lines:
        data = line.split(',')
        artist_name.append(data[1])

    # split the user entered text for validation purposes
    keyword = enter.split(" ")
    keyword2 = enter.partition(keyword[1] + " ")[2]
    print("You entered:", keyword[0], keyword[1], keyword2)

    # if first keyword is artist, check second keyword
    if keyword[0] == "artist":
        if keyword[1] != "genre" and keyword[1] != "topsong" and keyword[1] != "numsongs":
            error_input()
        else:
            if (keyword2) in artist_name:
                # good input, continue to query
                get_artist_data(keyword[1], keyword2)
            else:
                # bad input, return error message 
                error_input2()
    else:
        error_input()


# song commands validation
def song_validation(enter):

    # open csv file and read song name column into a list
    with open("songs.csv", "r") as csv_file:
        lines = csv_file.readlines()
    song_name = []
    for line in lines:
        data = line.split(',')
        song_name.append(data[1])

    # split the user entered text for validation purposes
    keyword = enter.split(" ")
    keyword2 = enter.partition(keyword[1] + " ")[2]
    print("You entered: ", keyword[0], " ", keyword[1], " ", keyword2)

    # if first keyword is song, check second keyword
    if keyword[0] == "song":
        if keyword[1] != "length" and keyword[1] != "numreplays" and keyword[1] != "artist":
            error_input()
        else:
            if (keyword2) in song_name:
                # good input, continue to query
                result = get_song_data(keyword[1], keyword2)
            else:
                # bad input, return error message
                error_input2()
    else:
        error_input()

def both_validation(enter):
    # song name ____ artist name _____ length

    with open("artists.csv", "r") as csv_file:
        lines = csv_file.readlines()
    artist_name = []
    for line in lines:
        data = line.split(',')
        artist_name.append(data[1])

    keyword = enter.split(" ")
    keyword2 = enter.partition(keyword[1] + " ")[2]
    print("You entered:", keyword[0], keyword[1], keyword2)

    # if first keyword is artist, check second keyword
    if keyword[0] == "artist":
        if keyword[1] != "name":
            error_input()
        else:
            if (keyword2) in artist_name:
                # good input, continue to query
                foreign_key(keyword2)
            else:
                # bad input, return error message 
                error_input2()
    else:
        error_input()
   