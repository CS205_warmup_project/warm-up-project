# Import required libraries
import sqlite3
import csv

def open_database():
    try:
        #check if the database already exists
        # if is_SQLite3('database.db'):
            # Connect to  database
        global conn
        conn = sqlite3.connect('database.db')
        # #Create tables
        # print ("Database opened successfully")
        # load_data()
        # print("Data loaded")
        return conn
        # else:
        #     #if database already exists, just open it
        #     conn = sqlite3.connect('database.db')
        #     print ("Database opened successfully")
        #     return conn
    except:
        print("Unable to load database")

def load_data():
    # Connecting to the geeks database
    connection = open_database()

    # Creating a cursor object to execute
    # SQL queries on a database table
    cursor = connection.cursor()

    load_artists(cursor)
    connection.commit()

    load_songs(cursor)
    connection.commit()
 
def load_artists(cursor):
    try:
        # Table Definition
        create_table = '''CREATE TABLE ARTISTS
                            (ID            INT          NOT NULL    PRIMARY KEY,
                            ARTIST         CHAR(50)     NOT NULL,
                            NUMSONGS       INT          NOT NULL,
                            GENRE          CHAR(50)     NOT NULL,
                            TOPSONG        CHAR(50)     NOT NULL);'''

        # Creating the table into our 
        # database
        cursor.execute(create_table)
        # Opening the person-records.csv file
        file = open('artists.csv')

        # Reading the contents of the 
        # person-records.csv file
        contents = csv.reader(file)

        # SQL query to insert data into the
        # person table
        insert_records = "insert into artists (id, artist, numsongs, genre, topsong) VALUES (?, ?, ?, ?, ?)"

        # Importing the contents of the file 
        # into our person table
        cursor.executemany(insert_records, contents)

        # SQL query to retrieve all data from
        # the person table To verify that the
        # data of the csv file has been successfully 
        # inserted into the table
        select_all = "SELECT * FROM artists"
        rows = cursor.execute(select_all).fetchall()

        # Output to the console screen
        for r in rows:
            print(r)
    except: 
        None
        print("Artist table already exists")
        
def load_songs(cursor):
    try:
        # Table Definition
        create_table = '''CREATE TABLE SONGS
                         (ID           INT         NOT NULL    PRIMARY KEY,
                          NAME         CHAR(50)    NOT NULL,
                          LENGTH       INT         NOT NULL,
                          NUMREPLAYS   CHAR(50)    NOT NULL,
                          ARTIST       CHAR(50)    NOT NULL    REFERENCES ARTISTS);'''

        # Creating the table into our 
        # database
        cursor.execute(create_table)
        # Opening the person-records.csv file
        file = open('songs.csv')

        # Reading the contents of the 
        # person-records.csv file
        contents = csv.reader(file)

        # SQL query to insert data into the
        # person table
        insert_records = "insert into songs (id, name, length, numreplays, artist) VALUES (?, ?, ?, ?, ?)"

        # Importing the contents of the file 
        # into our person table
        cursor.executemany(insert_records, contents)

        # SQL query to retrieve all data from
        # the person table To verify that the
        # data of the csv file has been successfully 
        # inserted into the table
        select_all = "SELECT * FROM songs"
        rows = cursor.execute(select_all).fetchall()

        # Output to the console screen
        for r in rows:
            print(r)
    except: 
        print("Song table already exists")
        None
        
#if the database doesn't exist, return true
def is_SQLite3(filename):
    from os.path import isfile, getsize

    if not isfile(filename):
        return True
    if getsize(filename) < 100: # SQLite database file header is 100 bytes
        return True
    return False

def close_database():
    conn.close()

def delete_tables(i):
    open_database()
    if i == 1:
        conn.execute("DROP TABLE ARTISTS")
    if i == 0:
        conn.execute("DROP TABLE SONGS")
    print("Tables deleted")
    conn.close()

def get_artist_data(column, artist):
    cursor = conn.cursor()
    query = ("SELECT " + column + " FROM ARTISTS WHERE ARTIST='" + artist + "';")
  #  print(query)
    data = cursor.execute(query).fetchone()
    print(data[0])

def get_song_data(column, song):
    cursor = conn.cursor()
    query = ("SELECT " + column + " FROM SONGS WHERE NAME='" + song + "';")
  #  print(query)
    data = cursor.execute(query).fetchone()
    print(data[0])

def foreign_key(artist):
    cursor = conn.cursor()
    query = ("SELECT SONGS.NAME FROM SONGS, ARTISTS WHERE ARTISTS.ARTIST = SONGS.ARTIST AND ARTISTS.ARTIST ='" + artist +  "';")
    records = cursor.execute(query).fetchall()
    for record in records:
        print(record[0])

def update_artists_table(row):
    with open('artists.csv', 'r') as csvfile: 
        row_count = sum(1 for row in csvfile)
        row.insert(0, row_count + 1)

    with open('artists.csv', 'a') as csvfile: 
        # creating a csv writer object 
        csvwriter = csv.writer(csvfile)             
        # writing the data rows
        csvwriter.writerow(row)
    
    delete_tables(1)
    load_data()
    csvfile.close()
    print("Data has been added to the database")

def update_songs_table(row):
    with open('songs.csv', 'r') as csvfile: 
        row_count = sum(1 for row in csvfile)
        row.insert(0, row_count + 1)
    
    with open('songs.csv', 'a') as csvfile: 
        # creating a csv writer object 
        csvwriter = csv.writer(csvfile)             
        # writing the data rows
        csvwriter.writerow(row) 

    delete_tables(0)
    load_data()
    csvfile.close()
    print("Data has been added to the database")

def get_metadata(table):
    cursor = conn.cursor()
    cursor.execute("select * from " + table + ";")
    results = cursor.fetchall()
    print(len(results))

def display_artists():
    cursor = conn.cursor()
    query = ("SELECT ARTIST FROM ARTISTS;")
    records = cursor.execute(query).fetchall()
    for record in records:
        print(record[0])

def reset_tables():
    open_database()
    delete_tables(0)
    delete_tables(1)
    load_data()
    close_database()