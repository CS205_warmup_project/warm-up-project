import query
#from parser import *

import parser


def print_help():
    # print out help text about commands
    print("Help Menu")
    print("------------------")
    print("Artist Data:")
    print(
        "1. To obtain the genre that an artist is assocaited with, please type 'artist genre' \nfollowed by the name of the artist you select.")
    print(
        "2. To obtain the top song of an artist, please type 'artist topsong' followed by the \nname of the artist you select.")
    print(
        "3. To obtain the number of songs that an artist has released, please type 'artist numsongs' \nfollowed by the name of the artist you select.")
    print("Song Data:")
    print(
        "1. To obtain the number of times a song has been played, please type 'song numreplays' \nfollowed by the name of the song you select.")
    print("2. To obtain the length of a song, please type 'song length' followed by the name of \nthe song you select.")
    print("3. To obtain the artist of a song, please type 'song artist' followed by the name of \nthe song you select.")
    print("Data for Both:")
    print(
        "If you know an artist and want to see all their songs, type 'artist name' + name of artist.")



def error_input():
    print("Error: your input is invalid, please check help menu.")



def error_input2():
    print("Your artist or song name was not recognized. Please try again or check the help menu.")


def quit_program():
    print("Would you like to enter another query (y or n)?")
    again = str(input("> "))
    if again == "y" or again == "Y":
        return True
    else:
        return False


def interface():
    flag = True
    load_data_flag = True
    print("Welcome\n")
    print("Please enter 'load data' for loading data")
    while load_data_flag:
        load = str(input("> "))
        if load == "load data":
            query.load_data()
            print("Success loading data.")
            load_data_flag = False
        else:
            print("You must enter 'load data' before you can continue.")
            load_data_flag = True

    while flag:
        print("Please enter a query to return data. If you do not know any queries, enter 'help' for the help menu.")
        enter = str(input("> "))
        # print help menu
        if enter == "help":
            print_help()
            flag = quit_program()
        else:
            keyword = enter.split(" ")
            if "artist name" in enter:
                parser.both_validation(enter)
                flag = quit_program()
            elif keyword[0] == "artist":
                parser.artist_validation(enter)
                flag = quit_program()
            elif keyword[0] == "song":
                parser.song_validation(enter)
                flag = quit_program()
            else:
                print(keyword[0])
                error_input()
                flag = quit_program()


if __name__ == '__main__':
    interface()
