**Rachel Liston, Haoyuan Pang, Ethan West, Shannon Lyons**

*CS 205 Warmup Project:*

Our query language enables users to type in certain commands and retrieve information
surrounding song and artist data in Spotify.

To begin, run 'interface.py'.

Using our query language: 

 * Before you search for song or artist data, please type 'load data' to load song.csv and artist.csv into 
 the tables.

* To search for information about an artist:
    * type '*artist*' followed by ... 
        * '*genre*' and the name of the song to obtain the artist's genre of music 
            * E.g. '*artist genre Elton John*' returns Elton John's genre of music
        * '*topsong*' and the name of the song to obtain the artists most-played song on Spotify
        * '*numsongs*' and the name of the song to obtain the number of the songs the artist published on Spotify
        
* To search for information about a song:
    * type '*song*' followed by ... 
        * '*length*' and the name of the song to obtain the length of the song in seconds
            * E.g. '*song length Rocket Man*' returns the length of the song, Rocket Man
        * '*numreplays*' and the name of the song to obtain the number of times the song has been played on Spotify
        * '*artist*' and the name of the song to obtain the name of the artist who wrote the song
        
 * To gather information from both the artist and song databases, enter '*artist name*', followed by the name of the artist. 
        
 * If you would like to view the help menu for assistance with our 
    query language, type '*help*'.
    
  **NOTE**: Please type in the name of your desired artists and songs **WITHOUT** quotes.